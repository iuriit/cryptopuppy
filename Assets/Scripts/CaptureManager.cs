﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptureManager : MonoBehaviour {

    public GameObject ui;
    public GameObject logo;
    public GameObject preview;

    CaptureAndSave snapShot;

    // Use this for initialization
    void Start()
    {
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
    }

    #region SnapShot

    void OnEnable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
        CaptureAndSaveEventListener.onScreenShot += OnScreenShot;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onError -= OnError;
        CaptureAndSaveEventListener.onSuccess -= OnSuccess;
        CaptureAndSaveEventListener.onScreenShot -= OnScreenShot;
    }

    void OnError(string error)
    {
        Debug.Log("Error : " + error);
        ui.SetActive(true);
        logo.SetActive(false);
    }

    void OnSuccess(string msg)
    {
        Debug.Log("Success : " + msg);
        ui.SetActive(true);
        logo.SetActive(false);
    }

    void OnScreenShot(Texture2D tex)
    {
        ui.SetActive(true);
        logo.SetActive(false);
        preview.SetActive(true);

        preview.GetComponent<Image>().sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
    }

    #endregion SnapShot

    #region UI Handlers

    public void OnCapture()
    {
        ui.SetActive(false);
        logo.SetActive(true);
        snapShot.GetFullScreenShot(ImageType.JPG);
    }

    public void OnClose()
    {
        preview.SetActive(false);
    }

    public void OnSave()
    {
        snapShot.SaveTextureToGallery(preview.GetComponent<Image>().sprite.texture, ImageType.JPG);
        preview.SetActive(false);
    }

    #endregion UI Handlers
}
