﻿using Assets.Scripts.Models;
using System;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

namespace Assets.Scripts
{
    public class ApiRoute
    {
        public static string PuppyTap = "user/tap";
        public static string SignUp = "user/create";
        public static string SignIn = "user/login";
        public static string Profile = "user/profile";
        public static string Leaders = "user/leaders";
    }

    public class Api
    {
        const string TokenPrefsKey = "e4b2a-35a9331b-9acf";
        public static string Host = "http://localhost/api/";

        public static string Token
        {
            get
            {
                return PlayerPrefs.GetString(TokenPrefsKey, null);
            }

            set
            {
                PlayerPrefs.SetString(TokenPrefsKey, value);
            }
        }

        public static bool IsAuthenticated
        {
            get
            {
                return !string.IsNullOrEmpty(PlayerPrefs.GetString(TokenPrefsKey, null));
            }
        }

        public static void SignIn(Action<DataModel> succes, Action<string> error, params string[] args)
        {
            Post(succes, error, ApiRoute.SignIn, args);
        }

        public static void SingUp(Action<DataModel> succes, Action<string> error, params string[] args)
        {
            Post(succes, error, ApiRoute.SignUp, args);
        }

        public static void Tap(Action<DataModel> succes, Action<string> error)
        {
            Post(succes, error, ApiRoute.PuppyTap, "token", Token);
        }

        public static void Profile(Action<DataModel> succes, Action<string> error)
        {
            Post(succes, error, ApiRoute.Profile, "token", Token);
        }

        public static void Leaders(Action<DataModel> succes, Action<string> error)
        {
            Post(succes, error, ApiRoute.Leaders, "token", Token);
        }

        public static void Post(Action<DataModel> succes, Action<string> error, string route, params string[] args)
        {
            UnityWebRequest unityWebRequest = UnityWebRequest.Post(Host + route, GetForm(args));

            var operation = unityWebRequest.SendWebRequest();

            operation.completed += (AsyncOperation obj) =>
            {
                if (unityWebRequest.isHttpError || unityWebRequest.isNetworkError)
                {
                    error(unityWebRequest.error);
                }
                else
                {
                    var dataModel = DataModel.Parse(unityWebRequest.downloadHandler.text);

                    if (dataModel.code == 200)
                    {
                        succes(dataModel);
                    }
                    else
                    {
                        error(dataModel.error);
                    }
                }
                
            };
        }

        public static bool IsValidEmail(string email)
        {
            return !string.IsNullOrEmpty(email) && 
                Regex.IsMatch(email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }


        private static WWWForm GetForm(string[] args)
        {

            var form = new WWWForm();


            if (args != null && args.Length % 2 == 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    form.AddField(args[i], args[++i]);
                }
            }

            return form;
        }

        private static Uri GetUri(string action, string[] args = null)
        {
            UriBuilder builder = new UriBuilder(Host + action);
            StringBuilder query = new StringBuilder();

            // checking if args lins is a factor of 2
            // to have key value pair
            if (args != null && args.Length % 2 == 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (query.Length > 0)
                        query.Append("&");

                    query.Append(args[i]);
                    query.Append("=");
                    query.Append(args[++i]);
                }
            }

            builder.Query = query.ToString();

            return builder.Uri;
        }

        private static NameValueCollection GetCollection(string[] args)
        {
            var res = new NameValueCollection();

            if (args != null && args.Length % 2 == 0)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    res.Add(args[i], args[++i]);
                }
            }

            return res;
        }
    }
}
