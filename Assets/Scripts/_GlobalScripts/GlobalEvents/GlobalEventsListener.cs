﻿using System;
using UnityEngine;
namespace _GameSys
{


    public class GlobalEventsListener : MonoBehaviour
    {

        static event Action<SimpleNotificationId> SimpleNotificationEvent = //delegate (SimpleNotificationId id) { Debug.Log("Notification: " + id); };
            delegate { };


        static event Action<VectorDataNotificationId,Vector3> VectorNotificationEvent =   delegate { };

        static event Action<StringDataNotificationId, string> StringDataEvent = //delegate (StringDataNotificationId id, string data) { Debug.Log("StringDataNotificatio: " + id+ ", data "+ data); };
         delegate { };

        static public void Subscribe(Action<StringDataNotificationId,string> newHandler)
        {
            StringDataEvent += newHandler;
        }

        static public void Subscribe(Action<VectorDataNotificationId, Vector3> newHandler)
        {
            VectorNotificationEvent += newHandler;
        }

        static public void Subscribe(Action<SimpleNotificationId> newHandler)
        {
            SimpleNotificationEvent += newHandler;
        }

        static public void UnSubscribe(Action<VectorDataNotificationId, Vector3> newHandler)
        {
            VectorNotificationEvent -= newHandler;
        }

        static public void UnSubscribe(Action<SimpleNotificationId> newHandler)
        {
            SimpleNotificationEvent -= newHandler;
        }

        static public void UnSubscribe(Action<StringDataNotificationId, string> newHandler)
        {
            StringDataEvent -= newHandler;
        }


        public static void Notify(VectorDataNotificationId id, Vector3 val)
        {
            VectorNotificationEvent(id,val);
        }

        public static void Notify(SimpleNotificationId id )
        {
            SimpleNotificationEvent(id);
        }

        public static void Notify(StringDataNotificationId id, string data)
        {
            StringDataEvent(id, data);
        }

    }
}
