﻿using UnityEngine;
using System;

namespace _GameSys
{


    public class AllEventsListener : GlobalEventsListener
    {

        protected virtual void OnEnable()
        {
            Subscribe(OnNotification);
            Subscribe(OnStringDataNotification);

        }

        protected virtual void OnDisable()
        {
            UnSubscribe(OnNotification);
            UnSubscribe(OnStringDataNotification);
        }


        protected virtual void OnNotification(SimpleNotificationId id)
        {
            Debug.LogWarning(" not overriden OnNotfication got SimpleNotificationId  " + id + " gameObject " + gameObject.name + " component " + this.GetType());
        }

        protected virtual void OnStringDataNotification(StringDataNotificationId id, string data)
        {
            Debug.LogWarning(" not overriden OnNotfication got StringDataNotificationId  " + id + " data " + data + " gameObject "
                + gameObject.name + " component " + this.GetType());

            switch (id)
            {

                default:
                    break;

            }
        }
    }
}
