﻿using UnityEngine;
using System;

namespace _GameSys
{


    public class StringDataEventsListener : GlobalEventsListener
    {

        protected virtual void OnEnable()
        {
            Subscribe(OnStringDataNotification);
        }

        protected virtual void OnDisable()
        {
            UnSubscribe(OnStringDataNotification);
        }

        protected virtual void OnStringDataNotification(StringDataNotificationId id, string data)
        {
            Debug.LogWarning(" not overriden OnNotfication got StringDataNotificationId  " + id + " data " + data + " gameObject "
                + gameObject.name + " component " + this.GetType());

            switch (id)
            {

                default:
                    break;

            }
        }


    }
}
