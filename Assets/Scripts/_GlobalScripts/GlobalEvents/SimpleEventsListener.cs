﻿using UnityEngine;
using System;

namespace _GameSys
{


    public class SimpleGlobalEventsListener : GlobalEventsListener
    {

        protected virtual void OnEnable()
        {
            Subscribe(OnNotification);
        }

        protected virtual void OnDisable()
        {
            UnSubscribe(OnNotification);
        }


        protected virtual void OnNotification(SimpleNotificationId id)
        {
            Debug.LogWarning(" not overriden OnNotfication got SimpleNotificationId  " + id + " gameObject " + gameObject.name + " component " + this.GetType());
        }

    }
}
