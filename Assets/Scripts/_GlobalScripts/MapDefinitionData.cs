﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _GameSys
{

    /// <summary>
    /// Helper to store position of marker and then apply it instantiated object
    /// </summary>
    [System.Serializable]
    public class PlaceDefinition
    {
        public string TypeId = "door";
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 Scale;

        public PlaceDefinition() { }
        /// <summary>
        /// Remember the position of marker
        /// </summary>
        /// <param name="item">Marker</param>
        public PlaceDefinition(Transform item)
        {
            Position = item.position;
            Rotation = item.rotation;
            Scale = item.localScale;
        }

        /// <summary>
        /// Apply definition to instantiated object
        /// </summary>
        /// <param name="clone">instantiated object</param>
        public void Apply(Transform clone)
        {

            clone.position = Position;
            clone.rotation = Rotation;
            clone.localScale = Scale;
        }

        public Vector3 DirectionToOtherPlace(PlaceDefinition target)
        {
            return (target.Position - Position);
        }


    }
    /// <summary>
    /// Fill the places at map with prefab clones
    /// </summary>
    [System.Serializable]
    public class MapDefinitionData
    {
        public List<PlaceDefinition> Definitions = new List<PlaceDefinition>();

        /// <summary>
        /// generate clones as child of places root
        /// </summary>
        /// <param name="prefab"></param>
        /// <param name="rootOfPlaces"></param>
        public void Generate(GameObject prefab, GameObject rootOfPlaces)
        {

            if (Definitions == null || Definitions.Count == 0 || prefab == null)
                return;
            foreach (var data in Definitions)
            {
               var go =  GameObject.Instantiate(prefab, data.Position, data.Rotation, rootOfPlaces.transform);
                go.transform.localScale = data.Scale;
            }

        }

        public void Apply(PlaceDefinition def, GameObject clone)
        {
            def.Apply(clone.transform);
        }

        /// <summary>
        ///  when instead alligning of clone with its zero it was used the special marker for its pivot 
        /// </summary>
        /// <param name="def">definition of place</param>
        /// <param name="cloneRoot"></param>
        /// <param name="clonePivot"></param>
        public void Apply(PlaceDefinition def, Transform cloneRoot, Transform clonePivot)
        {

            Quaternion relativeRotation =  Quaternion.Inverse(clonePivot.rotation) * def.Rotation;
            cloneRoot.position = cloneRoot.position + (def.Position - clonePivot.position);
            cloneRoot.rotation = clonePivot.rotation * relativeRotation;
            cloneRoot.localScale = def.Scale;


        }

        public PlaceDefinition GetAny()
        {
            if (Definitions.Count == 0)
                return null;

            return Definitions[0];
        }


        public void Add(PlaceDefinition newItem)
        {
            Definitions.Add(newItem);
        }

        public void Clear()
        {
            Definitions.Clear();
        }


        public void FillWithTestData()
        {
            for (int i = 0; i < 5; i++)
            {
                PlaceDefinition newItem = new PlaceDefinition();
                newItem.Position = new Vector3(2 * i, 0, 3 * i + 3);
                newItem.Rotation = Quaternion.identity;
                newItem.Scale = new Vector3((i + 1) * 0.5f, (i + 1) * 0.5f, (i + 1) * 0.5f);
                _Sys.MapDefinition.Definitions.Add(newItem);
            }

        }

    }
}
