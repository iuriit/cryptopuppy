﻿using UnityEngine;
using UnityEngine.UI;

namespace CyberPuppy
{
    public class LeaderboardButton : MonoBehaviour
    {
        public Button button;

        // Use this for initialization
        void Start()
        {
            if (button == null)
                button = GetComponent<Button>();

            button.onClick.AddListener(OnButtonClicked);
        }

        void OnButtonClicked()
        {
            GameManager.LoadScene(GameScene.LeaderBoard);
        }
        // Update is called once per frame
        void Update()
        {

        }
    }
}
