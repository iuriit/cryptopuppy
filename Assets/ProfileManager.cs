﻿/// <summary>
/// Created by Alex Poloboc, 17.02.2018
/// poloboc@live.com
/// </summary>

using Assets.Scripts;
using Assets.Scripts.Models;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class ProfileManager : MonoBehaviour {
    public const string EMAIL_ADDRESS_KEY = "email-address";
    public const string USER_NAME_KEY = "user-name";
    public const string PUPPY_NAME_KEY = "puppy-name";
    public const string WALLET_ADDRESS_KEY = "walled-address";
    public const string USER_INITIALS_KEY = "user-initials";

    public Text TopUserInitials;
    public Text TopEmailAddress;
    public Text TopUserName;
    public Text ContextUserName;
    public Text ContextEmailAddress;
    public Text ContextPuppyName;
    public Text ContextWalletAddress;

    public void Start()
    {
        ContextUserName.text = UserName;
        ContextEmailAddress.text = EmailAddress;
        ContextPuppyName.text = PuppyName;
        ContextWalletAddress.text = WalletAddress;
        TopUserInitials.text = ExtractInitials(UserName);
        TopUserName.text = UserName;
        TopEmailAddress.text = EmailAddress;
        

        Api.Profile((response) =>
        {
            UserName = response.data.user.name;
            EmailAddress = response.data.user.email;
            PuppyName = response.data.user.puppy_name;
            WalletAddress = response.data.user.wallet;
        },
        (message) =>
        {

        });
    }

    public string Initials
    {
        get
        {
            return PlayerPrefs.GetString(USER_INITIALS_KEY, "");
        }
        set
        {
            TopUserInitials.text = value;
            PlayerPrefs.SetString(USER_INITIALS_KEY, value);
        }
    }

    public string EmailAddress
    {
        get
        {
            return PlayerPrefs.GetString(EMAIL_ADDRESS_KEY, "");
        }
        set
        {
            TopEmailAddress.text = value;
            ContextEmailAddress.text = value;
            PlayerPrefs.SetString(EMAIL_ADDRESS_KEY, value);
        }
    }

    public string PuppyName
    {
        get
        {
            return PlayerPrefs.GetString(PUPPY_NAME_KEY, "");
        }
        set
        {
            ContextPuppyName.text = value; 
            PlayerPrefs.SetString(PUPPY_NAME_KEY, value);
        }
        
    }

    public string UserName
    {
        get
        {
            return PlayerPrefs.GetString(USER_NAME_KEY, "");
        }
        set
        {
            TopUserInitials.text = ExtractInitials(value);
            TopUserName.text = value;
            ContextUserName.text = value;
            PlayerPrefs.SetString(USER_NAME_KEY, value);
        }

    }

    public string WalletAddress
    {
        get
        {
            return PlayerPrefs.GetString("wallet-address", "");
        }
        set
        {
            ContextWalletAddress.text = value;
            PlayerPrefs.SetString("wallet-address", value);
        }

    }

    public static void UpdatePrefs(User user)
    {
        PlayerPrefs.SetString(EMAIL_ADDRESS_KEY, user.email);
        PlayerPrefs.SetString(USER_NAME_KEY, user.name);
        PlayerPrefs.SetString(PUPPY_NAME_KEY, user.puppy_name);
        PlayerPrefs.SetString(WALLET_ADDRESS_KEY, user.wallet);
        PlayerPrefs.SetString(USER_INITIALS_KEY, user.wallet);
    }

    public static string ExtractInitials(string name)
    {
        // first remove all: punctuation, separator chars, control chars, and numbers (unicode style regexes)
        string initials = Regex.Replace(name, @"[\p{P}\p{S}\p{C}\p{N}]+", "");

        // Replacing all possible whitespace/separator characters (unicode style), with a single, regular ascii space.
        initials = Regex.Replace(initials, @"\p{Z}+", " ");

        // Remove all Sr, Jr, I, II, III, IV, V, VI, VII, VIII, IX at the end of names
        initials = Regex.Replace(initials.Trim(), @"\s+(?:[JS]R|I{1,3}|I[VX]|VI{0,3})$", "", RegexOptions.IgnoreCase);

        // Extract up to 2 initials from the remaining cleaned name.
        initials = Regex.Replace(initials, @"^(\p{L})[^\s]*(?:\s+(?:\p{L}+\s+(?=\p{L}))?(?:(\p{L})\p{L}*)?)?$", "$1$2").Trim();

        if (initials.Length > 2)
        {
            // Worst case scenario, everything failed, just grab the first two letters of what we have left.
            initials = initials.Substring(0, 2);
        }

        return initials.ToUpperInvariant();
    }
}
