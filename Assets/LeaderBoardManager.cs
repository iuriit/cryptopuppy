﻿using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Created by Alex Poloboc, 18.02.2018
/// poloboc@live.com
/// </summary>

public class LeaderBoardManager : MonoBehaviour {

    public ScrollRect scrollRect;
    public VerticalLayoutGroup verticalLayoutGroup;
    public GameObject itemTemplate;

    void Start()
    {
        Api.Leaders((response) =>
        {
            GameObject newItem;

            int leaderCount = response.data.leaders.Count;

            var rectTransform = verticalLayoutGroup.GetComponent<RectTransform>();

            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, leaderCount * 185);
            
            for (int i = 0; i < leaderCount; i++)
            {
                var leader = response.data.leaders[i];

                if (i == 0)
                    newItem = itemTemplate;
                else
                    newItem = Instantiate(itemTemplate, verticalLayoutGroup.transform);

                var components = newItem.GetComponentsInChildren(typeof(Component));

                foreach (var component in components)
                {
                    if (component.tag == "Initials" && component is Text)
                    {
                        ((Text)component).text = ProfileManager.ExtractInitials(leader.name);
                    }

                    if (component.tag == "UserName" && component is Text)
                    {
                        ((Text)component).text = leader.name;
                    }

                    if (component.tag == "Score" && component is Text)
                    {
                        ((Text)component).text = leader.points.ToString("D4");
                    }
                }
            }
        }, (message) =>
        {

        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
