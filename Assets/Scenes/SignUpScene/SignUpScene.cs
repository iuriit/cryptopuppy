﻿/// <summary>
/// Created by Alex Poloboc, 17.02.2018
/// poloboc@live.com
/// </summary>

using Assets.Scripts;
using CyberPuppy;
using UnityEngine;
using UnityEngine.UI;

public class SignUpScene : MonoBehaviour {
    public Text DescriptionLabel;
    public Text PriceLabel;
    public InputField EmailInput;
    public InputField UserNameInput;
    public InputField PasswordInput;
    public Button SignUpButton;
    public Button BackButton;

    public void Start () {
        PasswordInput.inputType = InputField.InputType.Password;
        SignUpButton.onClick.AddListener(OnSignUpClicked);
        BackButton.onClick.AddListener(OnBackButtonClicked);
    }

    private void OnBackButtonClicked()
    {
        GameManager.LoadScene(GameScene.Login);
    }

    private void OnSignUpClicked()
    {
        if (string.IsNullOrEmpty(UserNameInput.text) && string.IsNullOrEmpty(EmailInput.text))
        {
            SetErrorMessage("Email, name and password input fileds can not be empty");

            return;
        }

        if (string.IsNullOrEmpty(EmailInput.text))
        {
            SetErrorMessage("Email input field can't be empty");

            return;
        }

        if (string.IsNullOrEmpty(UserNameInput.text))
        {
            SetErrorMessage("User name input field can't be empty");

            return;
        }

        if (string.IsNullOrEmpty(PasswordInput.text))
        {
            SetErrorMessage("Password input field can't be empty");

            return;
        }

        if (!Api.IsValidEmail(EmailInput.text))
        {
            SetErrorMessage("Please provide a valid email address");

            return;
        }

        Api.SingUp((response) =>
        {
            Api.Token = response.data.token;

            ProfileManager.UpdatePrefs(response.data.user);

            GameManager.LoadScene(GameScene.Character);
        }, (message) =>
        {
            SetErrorMessage(message);
        },
        "email", EmailInput.text,
        "name", UserNameInput.text,
        "password", UserNameInput.text);
    }

    void SetErrorMessage(string message)
    {
        DescriptionLabel.color = Color.red;
        DescriptionLabel.text = message;
        PriceLabel.text = "";
    }
}
