﻿/// <summary>
/// Created by Alex Poloboc, 17.02.2018
/// poloboc@live.com
/// </summary>

using Assets.Scripts;
using CyberPuppy;
using UnityEngine;
using UnityEngine.UI;

public class LoginScene : MonoBehaviour
{
    public InputField EmailInput;
    public InputField PasswordInput;
    public Text DescriptionText;
    public Text AppPriceText;
    public Button LoginButton;
    public Button SignUpButton;

    public void Start()
    {
        PasswordInput.inputType = InputField.InputType.Password;
        LoginButton.onClick.AddListener(OnLoginClicked);
        SignUpButton.onClick.AddListener(OnSignUpClicked);
    }

    public void OnLoginClicked()
    {
        if (string.IsNullOrEmpty(PasswordInput.text) && string.IsNullOrEmpty(EmailInput.text))
        {
            DisplayErrorMessage("Email and password input fileds can not be empty");

            return;
        }

        if (string.IsNullOrEmpty(PasswordInput.text))
        {
            DisplayErrorMessage("Password input field can't be empty");

            return;
        }

        if (string.IsNullOrEmpty(EmailInput.text))
        {
            DisplayErrorMessage("Email input field can't be empty");

            return;
        }

        if (!Api.IsValidEmail(EmailInput.text))
        {
            DisplayErrorMessage("Please provide a valid email address");

            return;
        }

        Api.SignIn((response) =>
        {
            Api.Token = response.data.token;

            ProfileManager.UpdatePrefs(response.data.user);

            GameManager.LoadScene(GameScene.Character);
        }, (message) =>
        {
            StartCoroutine("DisplayErrorMessage", message);
        },
        "email", EmailInput.text,
        "password", PasswordInput.text);
    }

    public void DisplayErrorMessage(string message)
    {
        DescriptionText.color = Color.red;
        DescriptionText.text = message;
        AppPriceText.text = "";
    }

    public void OnSignUpClicked()
    {
        GameManager.LoadScene(GameScene.SignUp);
    }
}