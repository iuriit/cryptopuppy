﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CharacterScene
{
    public class MessageTextBox : MonoBehaviour
    {
        public Text TextField;
   

        
        // Use this for initialization
        void Start()
        {
            if (TextField == null)
                TextField = GetComponentInChildren<Text>();
            TextField.text = string.Empty;

           // StartCoroutine(UpdateTextFieldValue(string.Empty));
        }

        bool DoSubscribe = false;
        private void OnEnable()
        {
            DoSubscribe = true;
        }

        IEnumerator UpdateTextFieldValue(string textVal)
        {
            yield return null;
            TextField.enabled = false;

            int val = 0;
            int.TryParse(textVal, out val);
            string sign = val > 0 ? "+" : "";

            TextField.text = sign + val;
            yield return new WaitForSeconds(0.1f);
            TextField.enabled = true;
        }

        void OnAction(GameActionId id, string text)
        {
            Debug.Log("MessageTextBox got Action:" +id + ", val:'"+text+"");
            if (id == GameActionId.UpdateLocalScore)
            {
               
                StartCoroutine(UpdateTextFieldValue(text));
            }
            
        }

        private void OnDisable()
        {
            CharacterSceneManager.UnSubscribe(OnAction);
        }
        // Update is called once per frame
        void Update()
        {
            if (DoSubscribe)
            {
                DoSubscribe = false;
                CharacterSceneManager.Subscribe(OnAction);
            }
        }
    }
}
