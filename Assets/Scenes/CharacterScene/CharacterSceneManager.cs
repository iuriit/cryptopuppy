﻿using CyberPuppy;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CharacterScene
{
    public enum GameActionId
    {
        UpdateGlobalScore,
        UpdateLocalScore,
        ShowMessage,
        PlaySound,
        StopSound,
    }


    public class CharacterSceneManager : Singleton<CharacterSceneManager>
    {
        public Text userNamelabel;
        public Text userEmailAddress;

        public SoundsLibrary SoundsLib;

        #region Game actions
        static event Action<GameActionId, string> DoGameAct = delegate { };


        IEnumerator NotifySubroutine(GameActionId id, string str)
        {
            yield return null;
            DoGameAct(id, str);
        }

        public static void Notify(GameActionId id, string str)
        {
            if (Instance != null && DoGameAct != null && Instance != null && Instance.enabled)
                Instance.StartCoroutine(Instance.NotifySubroutine(id, str));

        }

        public static void Subscribe(Action<GameActionId, string> OnAction)
        {
            DoGameAct += OnAction;
        }

        public static void UnSubscribe(Action<GameActionId, string> OnAction)
        {
            DoGameAct -= OnAction;
        }
        #endregion Game actions

        #region Puppy actions
        public ActionDefinitions PuppyActions;

        static event Action<PuppyActionId> DoPuppyAct = delegate { };
        // Use this for initialization



        void OnPuppyAction(PuppyActionId id)
        {
            var def = PuppyActions.GetById(id);
            GameData.IncreaseCredits(def.Credits);
        }

        public static void Subscribe(Action<PuppyActionId> OnAction)
        {
            DoPuppyAct += OnAction;
        }

        public static void UnSubscribe(Action<PuppyActionId> OnAction)
        {
            DoPuppyAct -= OnAction;
        }

        IEnumerator NotifySubroutine(PuppyActionId id)
        {
            yield return null;
            DoPuppyAct(id);
        }


        public static void Notify(PuppyActionId id)
        {
            if (Instance != null && DoPuppyAct != null && Instance != null && Instance.enabled)
                Instance.StartCoroutine(Instance.NotifySubroutine(id));

        }

        void ClearPuppyEvents()
        {
            Delegate[] delegates = DoPuppyAct.GetInvocationList();
            foreach (Delegate d in delegates)
            {
                DoPuppyAct -= (Action<PuppyActionId>)d;
            }
        }


        #endregion Puppy actions
        private void Start()
        {
            //PlayerPrefs.DeleteAll();
            //return;
            userNamelabel.text = PlayerPrefs.GetString("user-name", "");
            userEmailAddress.text = PlayerPrefs.GetString("email-address", "");


            DoPuppyAct += OnPuppyAction;

            Notify(GameActionId.UpdateGlobalScore, OnTouchCharacter.score.ToString("D4"));
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            ClearPuppyEvents();
        }
    }
}