﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterScene
{

    [System.Serializable]
    public class OneSound
    {
        public string SoundId;
        public AudioClip Sound;
    }

    [System.Serializable]
    public class SoundsLibrary 
    {
        public List<OneSound> SoundsData;

        public AudioClip GetSoundById(string id)
        {
            if (SoundsData == null || SoundsData.Count == 0 || string.IsNullOrEmpty(id))
                return null;

            

            var soundData = SoundsData.Find(el =>
            el != null && el.SoundId != null && id.Equals(el.SoundId, System.StringComparison.OrdinalIgnoreCase));

            AudioClip ret = (soundData == null? null: soundData.Sound);
            return ret;
        }


    }
}
