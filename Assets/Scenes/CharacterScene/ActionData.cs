﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CharacterScene
{

    // wagtail
    // frown
    // pant
    // bark
    // sad

    public enum PuppyActionId
    {
        idle,
        wagtail,
        frown,
        pant,
        bark,
        sad,
        undefined,
    }



    [System.Serializable]
    public class OneActionDef
    {
        public string _actionId;
        public string ActionId
        {
            get { return DeNull(_actionId, DefaultVal); }
        }

        public int Credits;
        string DeNull(string val, string defaultVal)
        { return string.IsNullOrEmpty(val)? defaultVal:val; }

        public string _animationId;
        public string AnimationId
        {
            get { return DeNull(_animationId, ActionId); }
        }

        //public AudioClip Sound;        

        public const string DefaultVal = "undefined";

      
        
        [Header("Condition to select")]
        [Range(0f,100f)]
        public float Min = 0;
        [Range(0f, 100f)]
        public float Max = 100;

    }


    [System.Serializable]
    public class ActionDefinitions
    {
        const float LowestRandom = 0f;
        const float HighestRandom = 100f;
        public OneActionDef UndefinedActionDef;

        public List<OneActionDef> Data;
        int seed = -1;
        public float LastRandomVal;
        public OneActionDef GetRandom()
        {
            return Data[0];


            //if (seed < 0)
            //{
            //    seed = System.DateTime.Now.Second;
            //    UnityEngine.Random.InitState(seed);
            //}


            //if (Data == null || Data.Count == 0)
            //{
            //    Debug.LogError("ActionDefinitions are not ready, return UndefinedActionDef");
            //    return UndefinedActionDef;
            //}

            //LastRandomVal = Random.Range(LowestRandom, HighestRandom);
            //var choicedDefs = Data.FindAll(el => el != null && el.Min <= LastRandomVal && LastRandomVal <= el.Max);

            //if (choicedDefs.Count == 0)
            //{
            //    Debug.Log("No range found for random value "+ LastRandomVal + "; returns UndefinedActionDef");
            //    return UndefinedActionDef;
            //}

            //Debug.Log("Found "+ choicedDefs.Count+ " which corresponds to "+ LastRandomVal);

            //if (choicedDefs.Count == 1)
            //    return choicedDefs[0];

            
            //int actionIdx = Random.Range(0, choicedDefs.Count);
            
            //return choicedDefs[actionIdx];
            
        }

        public OneActionDef GetById(PuppyActionId id)
        {
            return GetById(id.ToString());
        }
        public OneActionDef GetById(string id)
        {
            if (Data == null || Data.Count == 0 )
            {
                Debug.LogError("ActionDefinitions are not ready, return default");
                return UndefinedActionDef;
            }

            
            OneActionDef res = UndefinedActionDef;

            if (!string.IsNullOrEmpty(id) &&
                !id.Equals(PuppyActionId.undefined.ToString(),System.StringComparison.OrdinalIgnoreCase) 
                )
                res = Data.Find(el => 
                el != null && id.Equals(el.ActionId,System.StringComparison.OrdinalIgnoreCase));

       
            return res;

            
        }

        /*
        [Header("Condition to select")]
        [Range(0f,100f)]
        public float Min = 0;
        [Range(0f, 100f)]
        public float Max = 100;
        public int Credits = 0;
        */
        // wagtail
        // frown
        // pant
        // bark
        // sad
    }

    public static class ActionIdParser
    {



        public static PuppyActionId AsActionId(this string stringId)
        {
            var ret = PuppyActionId.undefined;
            try
            {
                ret = (PuppyActionId)System.Enum.Parse(typeof(PuppyActionId), stringId, true);
            }
            catch
            {
                Debug.LogError(" Unknown id '"+ stringId+ "' recognized as  " + ret);
            };
            return ret;
        }
    }

}
