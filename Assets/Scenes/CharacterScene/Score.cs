﻿using CyberPuppy;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CharacterScene
{
    public class Score : MonoBehaviour
    {
        public Text ScoreText;
        
        // Use this for initialization
        void Start()
        {
            if (ScoreText == null)
                ScoreText = GetComponentInChildren<Text>();
        }

        bool DoSubscribe = false;
        private void OnEnable()
        {
            DoSubscribe = true;
        }
			
        void OnAction(GameActionId id, string str)
        {
			ScoreText.text = str;

        }

        private void OnDisable()
        {
            CharacterSceneManager.UnSubscribe(OnAction);
        }
        // Update is called once per frame
        void Update()
        {
            if (DoSubscribe)
            {
                DoSubscribe = false;
                CharacterSceneManager.Subscribe(OnAction);
            }
        }
    }
}
